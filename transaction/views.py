import django_filters
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError

from .serializer import TransactionSerializer, Transaction


class TransactionItemFilter(django_filters.FilterSet):
    date = django_filters.DateTimeFromToRangeFilter(
        label='Format date_after and date_before: "mm/dd/yyyy [%H:%M[:%S]]"')

    class Meta:
        model = Transaction
        fields = ['date', 'financial_account', 'category', 'recipient']


class TransactionViewSet(viewsets.ModelViewSet):

    serializer_class = TransactionSerializer
    filter_class = TransactionItemFilter

    def get_queryset(self):
        user = self.request.user
        return Transaction.objects.filter(user=user)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        new_fa_amount = instance.financial_account.amount - instance.amount

        if new_fa_amount < 0 and instance.financial_account.minus is False:
            raise ValidationError

        instance.financial_account.amount = new_fa_amount
        instance.financial_account.save()

        if instance.category:
            i_date = instance.date.strftime('%m.%Y')
            category_amount = instance.category.planing[i_date]['amount']
            instance.category.planing[i_date]['amount'] = float(category_amount) - float(instance.amount)
            instance.category.save()

        return super(TransactionViewSet, self).destroy(request, *args, **kwargs)
