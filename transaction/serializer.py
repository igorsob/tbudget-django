from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from datetime import datetime

from .models import Transaction
from category.models import Planing


def update_financial_account(plus_amount, financial_account):
    if financial_account.amount + plus_amount < 0 and financial_account.minus is False:
        raise ValidationError
    financial_account.amount += plus_amount
    financial_account.save()


def update_category(plus_amount, category, datetime_transaction):
    month, year = datetime_transaction.strftime('%m.%Y').split('.')
    planing, _ = Planing.objects.get_or_create(user=category.user, year=year, month=month, category=category)
    planing.amount += plus_amount
    planing.save()


def check_and_update_category(validated_data, instance):
    if instance.category and validated_data['category'] and validated_data['category'].id == instance.category.id:
        update_category(-instance.amount, instance.category, instance.date)
        update_category(validated_data['amount'], instance.category, validated_data['date'])
    else:
        if instance.category:
            update_category(-instance.amount, instance.category, instance.date)
        if validated_data['category']:
            update_category(validated_data['amount'], validated_data['category'], validated_data['date'])


class TransactionSerializer(serializers.ModelSerializer):

    date = serializers.DateTimeField(default=datetime.now)
    note = serializers.CharField(required=False)
    label = serializers.CharField(required=False)

    class Meta:
        model = Transaction
        fields = ('id', 'date', 'amount', 'note', 'label', 'financial_account', 'category', 'recipient')

    def create(self, validated_data):
        update_financial_account(validated_data['amount'], validated_data['financial_account'])

        try:
            if validated_data['category']:
                update_category(validated_data['amount'], validated_data['category'], validated_data['date'])
            transaction = Transaction(**validated_data)
            transaction.save()
            return transaction
        except Exception as e:
            update_financial_account(-validated_data['amount'], validated_data['financial_account'])
            if 'category' in validated_data:
                update_category(-validated_data['amount'], validated_data['category'], validated_data['date'])
            raise e

    def update(self, instance, validated_data):
        if 'amount' in validated_data:
            update_financial_account(-instance.amount + validated_data['amount'], instance.financial_account)

        check_and_update_category(validated_data, instance)

        instance.date = validated_data.get('date', instance.date)
        instance.amount = validated_data.get('amount', instance.amount)
        instance.note = validated_data.get('note', instance.note)
        instance.label = validated_data.get('label', instance.label)
        instance.category = validated_data.get('category', instance.category)

        instance.save()

        return instance


