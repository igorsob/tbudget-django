from django.db import models
from datetime import datetime
from financial_account.models import FinancialAccount
from category.models import Category
from recipient.models import Recipient
from django_currentuser.middleware import get_current_user
from django.contrib.auth import get_user_model


class Transaction(models.Model):

    date = models.DateTimeField(default=datetime.now)
    amount = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    note = models.CharField(max_length=200, default='')
    label = models.CharField(choices=(('red', 'red'), ('green', 'green'), ('blue', 'blue')), max_length=5, null=True)

    financial_account = models.ForeignKey(FinancialAccount, on_delete=models.CASCADE, related_name='transactions')
    category = models.ForeignKey(Category,  on_delete=models.SET_NULL, null=True, related_name='transactions')
    recipient = models.ForeignKey(Recipient, on_delete=models.SET_NULL, null=True, related_name='transactions')

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default=get_current_user)
