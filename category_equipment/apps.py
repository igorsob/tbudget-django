from django.apps import AppConfig


class CategoryEquipmentConfig(AppConfig):
    name = 'category_equipment'
