# Generated by Django 2.0.10 on 2019-02-02 20:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_currentuser.middleware


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('category_equipment', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='categoryequipment',
            name='user',
            field=models.ForeignKey(default=django_currentuser.middleware.get_current_user, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
