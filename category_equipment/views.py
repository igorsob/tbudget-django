from rest_framework import viewsets
from .serializer import CategoryEquipment, CategoryEquipmentSerializer


class CategoryEquipmentViewSet(viewsets.ModelViewSet):

    serializer_class = CategoryEquipmentSerializer

    def get_queryset(self):
        user = self.request.user
        return CategoryEquipment.objects.filter(user=user)
