from rest_framework import serializers
from .models import CategoryEquipment


class CategoryEquipmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = CategoryEquipment
        fields = ('id', 'name', 'equipments')
