from rest_framework import viewsets
from .serializer import Recipient, RecipientSerializer


class RecipientViewSet(viewsets.ModelViewSet):

    serializer_class = RecipientSerializer

    def get_queryset(self):
        user = self.request.user
        return Recipient.objects.filter(user=user)
