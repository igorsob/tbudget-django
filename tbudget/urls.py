from django.urls import path, include
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static

from front.views import front, script1, script5, script6, front_admin, script1_admin, script5_admin, script6_admin

from core.views import AuthViewSet, AdminViewSet

from financial_account.views import FinancialAccountViewSet
from transaction.views import TransactionViewSet
from category.views import CategoryViewSet
from recipient.views import RecipientViewSet
from note.views import NoteViewSet
from goal.views import GoalViewSet
from category_equipment.views import CategoryEquipmentViewSet
from equipment.views import EquipmentViewSet


router = routers.DefaultRouter()

router.register(r'auth', AuthViewSet, base_name='auth')

router.register(r'financialaccount', FinancialAccountViewSet, base_name='financialaccount')
router.register(r'transaction', TransactionViewSet, base_name='transaction')
router.register(r'categories', CategoryViewSet, base_name='categories')
# router.register(r'planing', PlaningViewSet, base_name='planing')
router.register(r'recipients', RecipientViewSet, base_name='recipients')
router.register(r'notes', NoteViewSet, base_name='notes')
router.register(r'goals', GoalViewSet, base_name='goals')
router.register(r'categoryequipment', CategoryEquipmentViewSet, base_name='categoryequipment')
router.register(r'equipment', EquipmentViewSet, base_name='equipment')
router.register(r'admin', AdminViewSet, base_name='admin_view')


urlpatterns = [
    path('', front),
    path('auth/signin', front),
    path('auth/signup', front),
    path('home', front),
    path('planing', front),
    path('transactions', front),
    path('financial-accounts', front),
    path('categories', front),
    path('recipients', front),
    path('equipment-categories', front),
    path('equipment/{id}', front),
    path('notes', front),
    path('goals', front),
    path('support', front),
    path('settings', front),
    path('admin/', front_admin),
    path('admin/home', front_admin),
    path('admin/home/', front_admin),
    #admin
    path('admin/1.633de8609160ec3b62ac.js', script1_admin),
    path('admin/5.378f1218063f88e8623e.js', script5_admin),
    path('admin/6.cff72c24959830b40632.js', script6_admin),

    path('1.f6cf733437c41d6c23cf.js', script1),
    path('5.ca418af80fe07d4f9333.js', script5),
    path('6.575f90eef03aa3290419.js', script6),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
