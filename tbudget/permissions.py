from rest_framework.permissions import BasePermission


class UserPermissions(BasePermission):

    def has_permission(self, request, view):
        return (request.user.email_verification and request.user.subscription > 0) or request.user.is_superuser


class AdminPermissions(BasePermission):

    def has_permission(self, request, view):
        return request.user.is_superuser
