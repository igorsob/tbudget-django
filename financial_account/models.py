from django.db import models
from django_currentuser.middleware import get_current_user
from django.contrib.auth import get_user_model


class FinancialAccount(models.Model):

    name = models.CharField(max_length=200, default='')
    amount = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    minus = models.BooleanField(default=False)

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default=get_current_user)
