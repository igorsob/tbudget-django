from rest_framework import viewsets

from .serializer import FinancialAccountSerializer, FinancialAccount


class FinancialAccountViewSet(viewsets.ModelViewSet):

    serializer_class = FinancialAccountSerializer

    def get_queryset(self):
        user = self.request.user
        return FinancialAccount.objects.filter(user=user)
