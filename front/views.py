from django.shortcuts import render, redirect


def front_admin(request):
    return render(request, 'admin.html')


def script1_admin(request):
    return render(request, '1.633de8609160ec3b62ac.js')


def script5_admin(request):
    return render(request, '5.378f1218063f88e8623e.js')


def script6_admin(request):
    return render(request, '6.cff72c24959830b40632.js')


def front(request):
    if request.user.is_authenticated or request.path_info in ('/auth/signin', '/auth/signup'):
        return render(request, 'client.html')
    return redirect('/auth/signin')


def script1(request):
    return render(request, '1.f6cf733437c41d6c23cf.js')


def script5(request):
    return render(request, '5.ca418af80fe07d4f9333.js')


def script6(request):
    return render(request, '6.575f90eef03aa3290419.js')
