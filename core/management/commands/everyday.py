from django.db.models import F
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model

from category.models import Category
from category_equipment.models import CategoryEquipment
from equipment.models import Equipment
from financial_account.models import FinancialAccount
from goal.models import Goal
from note.models import Note
from recipient.models import Recipient
from transaction.models import Transaction


class Command(BaseCommand):
    help = 'Every day script'

    def handle(self, *args, **options):
        User = get_user_model()
        for user in User.objects.filter(is_superuser=False, subscription__gt=0):
            user.subscription -= 1
            user.save()
        # User.objects.filter(is_superuser=False, subscription__gt=0).update(subscription=F('subscription') + 1)

        for user in User.objects.filter(is_superuser=False, subscription=0):
            user.delete_days -= 1
            user.save()
            if user.delete_days == 0:
                Category.objects.filter(user=user).delete()
                CategoryEquipment.objects.filter(user=user).delete()
                Equipment.objects.filter(user=user).delete()
                FinancialAccount.objects.filter(user=user).delete()
                Goal.objects.filter(user=user).delete()
                Note.objects.filter(user=user).delete()
                Recipient.objects.filter(user=user).delete()
                Transaction.objects.filter(user=user).delete()

        for user in User.objects.filter(is_superuser=False, subscription__in=[3, 10]):
            # sendEmail()
            pass

        for user in User.objects.filter(is_superuser=False, delete_days=3):
            # sendEmail()
            pass

        for user in User.objects.filter(is_superuser=False, delete_days=-30):
            # sendEmail()
            pass

        # for poll_id in args:
        #     try:
        #         poll = Poll.objects.get(pk=int(poll_id))
        #     except Poll.DoesNotExist:
        #         raise CommandError('Poll "%s" does not exist' % poll_id)
        #
        #     poll.opened = False
        #     poll.save()
        #
        #     self.stdout.write('Successfully closed poll "%s"' % poll_id)
