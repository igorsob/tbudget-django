from django.core.mail import send_mail
from django.core.validators import validate_email, ValidationError
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from rest_framework import viewsets
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from rest_framework.response import Response
from rest_framework.decorators import permission_classes, action
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token

from tbudget.permissions import AdminPermissions
from .models import User, UserPasswordRecoveryRequest, Common
from .serializers import (PasswordRecoverySerializer,
                          ChangePasswordSerializer,
                          LoginSerializer,
                          RegisterSerializer,
                          UserSerializer,
                          ProofEmailSerializer)
import logging


class AuthViewSet(viewsets.GenericViewSet):

    queryset = User.objects.all()
    http_method_names = ['post', 'get', 'put']

    def get_permissions(self):
        if self.action == 'profile':
            return super(AuthViewSet, self).get_permissions()
        self.permission_classes = [AllowAny]
        return super(AuthViewSet, self).get_permissions()

    def get_serializer_class(self):
        if self.action == 'password_recovery_request':
            return PasswordRecoverySerializer
        if self.action == 'password_recovery_process':
            return ChangePasswordSerializer
        if self.action == 'signup':
            return RegisterSerializer
        if self.action == 'profile':
            return UserSerializer
        if self.action == 'proof_email':
            return ProofEmailSerializer

        return LoginSerializer

    @action(methods=['get'], detail=False)
    def common(self, request):
        data = {1: '', 2: '', 3: '', 4: ''}
        common = Common.objects.filter(id__in=(1, 2, 3, 4))
        for item in common:
            data[item.id] = item.text
        return Response(data)

    @action(methods=['get', 'put'], detail=False)
    def profile(self, request):

        user_serializer = UserSerializer(instance=request.user)
        if request.data:
            user_serializer.update(request.user, request.data)
            if 'password' in request.data:
                request.user.set_password(request.data['password'])
                request.user.save()

        return Response(user_serializer.data)

    @action(methods=['post'], detail=False)
    # @permission_classes((AllowAny,))
    def signin(self, request):
        email = request.data.get("email")
        password = request.data.get("password")

        user = authenticate(email=email, password=password)

        if not user:
            raise NotFound
        auth_login(request, user)
        token, _ = Token.objects.get_or_create(user=user)

        return Response({'token': token.key})

    @action(methods=['post'], detail=False)
    # @permission_classes((AllowAny,))
    def signup(self, request):

        user_serializer = RegisterSerializer(data=request.data)
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.create(validated_data=user_serializer.validated_data)

        token = default_token_generator.make_token(user)

        uid = user.pk

        send_mail(
            'Proof email - tttbudget',
            f'username: {user.first_name}\ntoken: \t {token}\nuid: \t {uid}',
            'from@example.com',
            [str(user.email)],
            fail_silently=False,
        )

        return Response('Check email')

    @action(methods=['post'], detail=False)
    # @permission_classes((AllowAny, ))
    def proof_email(self, request):
        uidb64 = request.data.get("uid")
        token = request.data.get("token")

        try:
            pk = uidb64
            user = User.objects.get(pk=pk)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise NotFound

        if not default_token_generator.check_token(user, token):
            raise NotFound

        user.email_verification = True
        user.save()

        token, _ = Token.objects.get_or_create(user=user)

        return Response({'token': token.key})

    @action(methods=['post'], detail=False)
    # @permission_classes((AllowAny,))
    def password_recovery_request(self, request):
        email = request.data.get("email")

        try:
            validate_email(email)
        except ValidationError as e:
            return Response(str(e), status=HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist as e:
            return Response(str(e), status=HTTP_404_NOT_FOUND)

        try:
            token = default_token_generator.make_token(user)

            uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()

            send_mail(
                'Reset password - tttbudget',
                'username: {}\ntoken: \t {}\nuid: \t {}'.format(user.first_name, token, uid),
                'from@example.com',
                [str(user.email)],
                fail_silently=False,
            )

            uprr, _ = UserPasswordRecoveryRequest.objects.update_or_create(user=user, token=token)
            uprr.save()

        except Exception as e:
            return Response(str(e), status=HTTP_400_BAD_REQUEST)

        return Response({True})

    @action(methods=['post'], detail=False)
    # @permission_classes((AllowAny,))
    def password_recovery_process(self, request):
        new_password = request.data.get("new_password")
        confirm_password = request.data.get("confirm_password")
        uidb64 = request.data.get("uid")
        token = request.data.get("token")

        if new_password != confirm_password:
            raise ValidationError

        try:
            pk = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=pk)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise NotFound

        if not default_token_generator.check_token(user, token):
            raise NotFound

        try:
            user.set_password(new_password)
            user.save()

            UserPasswordRecoveryRequest.objects.filter(user=user).delete()

            token, _ = Token.objects.get_or_create(user=user)
        except Exception as e:
            return Response(str(e), status=HTTP_400_BAD_REQUEST)

        return Response({'token': token.key})


class AdminViewSet(viewsets.GenericViewSet):

    queryset = User.objects.all()
    http_method_names = ['post', 'get', 'put']
    permission_classes = (AdminPermissions, )
    serializer_class = UserSerializer

    @action(methods=['get'], detail=False)
    def profiles(self, request):
        users = User.objects.filter(is_superuser=False)
        user_serializer = UserSerializer(users, many=True)
        return Response(user_serializer.data)

    @action(methods=['put'], detail=False)
    def profile(self, request):
        User.objects.filter(id=request.data['id']).update(subscription=request.data['subscription'])
        return Response()

    @action(methods=['put'], detail=False)
    def common(self, request):
        item, _ = Common.objects.get_or_create(id=request.data['id'])
        item.text = request.data['text']
        item.save()
        return Response()
