from rest_framework import serializers
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)
    subscription = serializers.IntegerField(read_only=True)

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'password', 'first_name', 'last_name', 'subscription', 'mailing')


class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('email', 'password')


class PasswordRecoverySerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('email',)


class ChangePasswordSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField()
    confirm_password = serializers.CharField()
    uid = serializers.CharField()
    token = serializers.CharField()

    class Meta:
        model = get_user_model()
        fields = ('new_password', 'confirm_password', 'uid', 'token')


class ProofEmailSerializer(serializers.ModelSerializer):
    uid = serializers.CharField()
    token = serializers.CharField()

    class Meta:
        model = get_user_model()
        fields = ('uid', 'token')


class RegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name', 'password')

    def create(self, validated_data):
        user = get_user_model().objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
