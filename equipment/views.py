from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from .serializer import Equipment, EquipmentSerializer


class EquipmentViewSet(viewsets.ModelViewSet):

    serializer_class = EquipmentSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('category', )

    def get_queryset(self):
        user = self.request.user
        return Equipment.objects.filter(user=user)
