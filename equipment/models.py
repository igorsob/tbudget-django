from django.db import models
from category_equipment.models import CategoryEquipment
from django_currentuser.middleware import get_current_user
from django.contrib.auth import get_user_model


class Equipment(models.Model):

    name = models.CharField(max_length=200, default='')
    category = models.ForeignKey(CategoryEquipment, on_delete=models.SET_NULL, null=True, related_name='equipments')

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default=get_current_user)
