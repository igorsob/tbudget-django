from rest_framework import viewsets
from .serializer import Goal, GoalSerializer


class GoalViewSet(viewsets.ModelViewSet):

    serializer_class = GoalSerializer

    def get_queryset(self):
        user = self.request.user
        return Goal.objects.filter(user=user)
