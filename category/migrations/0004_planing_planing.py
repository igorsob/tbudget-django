# Generated by Django 2.0.10 on 2019-03-05 07:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0003_auto_20190302_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='planing',
            name='planing',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=50),
        ),
    ]
