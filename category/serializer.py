from rest_framework import serializers
from datetime import date
from dateutil.relativedelta import relativedelta

from .models import Category, Planing


def months_to_str(i):
    return (date.today() + relativedelta(months=i)).strftime('%m.%Y')


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name', 'planing')
        depth = 1

    # def create(self, validated_data):
    #     category = Category(**validated_data)
    #     category.planing = {months_to_str(i): {'amount': .0, 'planing': .0} for i in range(12)}
    #     category.save()
    #     return category


class PlaningSerializer(serializers.ModelSerializer):

    class Meta:
        model = Planing
        fields = ('id', 'year', 'month', 'planing', 'category', 'amount', 'user')
