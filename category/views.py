from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializer import Category, CategorySerializer, Planing, PlaningSerializer


def sort(x):
    s = x.split('.')
    return s[1], s[0]


class CategoryViewSet(viewsets.ModelViewSet):

    serializer_class = CategorySerializer

    def get_queryset(self):
        return Category.objects.filter(user=self.request.user)

    @action(['get', 'post'], True)
    def planing(self, request, pk):
        if request._request.method == 'GET':
            planing = Planing.objects.filter(user=self.request.user, category_id=pk)
            serializer = PlaningSerializer(instance=planing, many=True)
            return Response(serializer.data)
        else:
            month, year = request.data['date'].split('.')
            user = self.request.user
            planing, _ = Planing.objects.get_or_create(user=user, year=year, month=month, category_id=pk)
            planing.planing = request.data.get('planing', 0)
            planing.save()
            serializer = PlaningSerializer(instance=planing)
            return Response(serializer.data)

#
# class PlaningViewSet(viewsets.mixins.UpdateModelMixin, viewsets.mixins.ListModelMixin,
#                      viewsets.mixins.RetrieveModelMixin, viewsets.GenericViewSet):
#
#     serializer_class = PlaningSerializer
#
#     def get_queryset(self):
#         user = self.request.user
#         return Category.objects.filter(user=user)
#
#     def list(self, request, *args, **kwargs):
#         queryset = self.filter_queryset(self.get_queryset())
#
#         page = self.paginate_queryset(queryset)
#         if page is not None:
#             serializer = self.get_serializer(page, many=True)
#             return self.get_paginated_response(serializer.data)
#
#         data = self.get_serializer(queryset, many=True).data
#
#         for category in data:
#             category['planing'] = sorted(category['planing'], key=sort)
#
#         return Response(data)
