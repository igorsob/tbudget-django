from django.db import models
from django_currentuser.middleware import get_current_user
from django.contrib.auth import get_user_model


class Category(models.Model):

    name = models.CharField(max_length=200, default='')

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default=get_current_user)


class Planing(models.Model):

    month = models.IntegerField(default=0)
    year = models.IntegerField(default=0)

    amount = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    planing = models.DecimalField(max_digits=50, decimal_places=2, default=0)

    category = models.ForeignKey(Category, models.CASCADE, 'planing', null=True)

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default=get_current_user)

    class Meta:
        ordering = ['year', 'month']
