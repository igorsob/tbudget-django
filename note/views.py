from rest_framework import viewsets
from .serializer import Note, NoteSerializer


class NoteViewSet(viewsets.ModelViewSet):

    serializer_class = NoteSerializer

    def get_queryset(self):
        user = self.request.user
        return Note.objects.filter(user=user)
